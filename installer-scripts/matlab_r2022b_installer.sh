#!/bin/bash
#Install Matlab R2022b

zipfile=matlab_R2022b_glnxa64.zip


echo "Install Matlab R2022b (v913)"
cd $HOME/Downloads

if [ ! -e $zipfile ]; then
  curl -O http://www.lin4neuro.net/lin4neuro/neuroimaging_software_packages/${zipfile}
fi

mkdir matlab_R2022b
cd matlab_R2022b
unzip ../${zipfile}

#Install 
sudo ./install

# Change permission of pathdef.m
echo "change permission of pathdef.m so that user can save path"
sudo chmod 666 /usr/local/MATLAB/R2022b/toolbox/local/pathdef.m

echo "Finished!"

exit

