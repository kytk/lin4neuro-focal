#!/bin/bash

# Directory
mkdir ~/Downloads

# apt
sudo apt-get update

# rename and unzip
sudo apt-get install -y rename unzip

# DCMTK
sudo apt-get install -y dcmtk

# AFNI #####
#Install prerequisite packages
sudo apt-get install -y tcsh xfonts-base libssl-dev       \
                        python-is-python3                 \
                        python3-matplotlib                \
                        gsl-bin netpbm gnome-tweak-tool   \
                        libjpeg62 xvfb xterm vim curl     \
                        gedit evince eog                  \
                        libglu1-mesa-dev libglw1-mesa     \
                        libxm4 build-essential            \
                        libcurl4-openssl-dev libxml2-dev  \
                        libgfortran-8-dev libgomp1        \
                        gnome-terminal nautilus           \
                        gnome-icon-theme-symbolic         \
                        firefox xfonts-100dpi             \
                        r-base-dev

#make a symbolic link for libgsl.so for Ubuntu 20.04
sudo ln -s /usr/lib/x86_64-linux-gnu/libgsl.so.23 /usr/lib/x86_64-linux-gnu/libgsl.so.19

#Download AFNI binary and installer
cd $HOME/Downloads
curl -O https://afni.nimh.nih.gov/pub/dist/bin/linux_ubuntu_16_64/@update.afni.binaries

#Install to /usr/local/AFNIbin
sudo tcsh @update.afni.binaries -curl -package linux_ubuntu_16_64 -bindir /usr/local/AFNIbin -do_extras

cd $HOME

#.bash_aliases
grep AFNI ~/.bash_aliases > /dev/null
if [ $? -eq 1 ]; then
    echo ' ' >> ~/.bash_aliases
    echo '#AFNI' >> ~/.bash_aliases
    echo 'export PATH=$PATH:/usr/local/AFNIbin' >> ~/.bash_aliases
fi


#Make AFNI/SUMA profiles
cp /usr/local/AFNIbin/AFNI.afnirc $HOME/.afnirc

grep ahdir ~/.bash_aliases > /dev/null
if [ $? -eq 1 ]; then
  echo 'ahdir=$(apsearch -afni_help_dir)' >> ~/.bash_aliases
  echo 'if [ -f "$ahdir/all_progs.COMP.bash" ]; then' >> ~/.bash_aliases
  echo '  . $ahdir/all_progs.COMP.bash' >> ~/.bash_aliases
  echo 'fi' >> ~/.bash_aliases
fi

# END OF AFNI #####

# FSL #####
# libraries
sudo apt-get install -y libmng2 libmng-dev libpng-dev libjpeg62

echo "Download FSL. It takes time, so please be patient."
cd $HOME/Downloads
curl -O http://www.lin4neuro.net/lin4neuro/neuroimaging_software_packages/fsl-6.0.5.2.zip

cd /usr/local
sudo unzip ~/Downloads/fsl-6.0.5.2.zip

grep FSLDIR ~/.profile > /dev/null
if [ $? -eq 1 ]; then

cat <<'EOF' >> ~/.profile

# FSL Setup
FSLDIR=/usr/local/fsl
PATH=${FSLDIR}/bin:${PATH}
export FSLDIR PATH
. ${FSLDIR}/etc/fslconf/fsl.sh
EOF

fi

# END OF FSL #####

# MRIcroGL #####
curl -O http://www.lin4neuro.net/lin4neuro/neuroimaging_software_packages/MRIcroGL_linux.zip

cd /usr/local
sudo unzip ~/Downloads/MRIcroGL_linux.zip

grep MRIcroGL ~/.bash_aliases > /dev/null
if [ $? -eq 1 ]; then
    echo '' >> ~/.bash_aliases
    echo '# MRIcroGL' >> ~/.bash_aliases
    echo 'export PATH=$PATH:/usr/local/MRIcroGL' >> ~/.bash_aliases
    echo 'export PATH=$PATH:/usr/local/MRIcroGL/Resources' >> ~/.bash_aliases
fi

# END OF MRIcroGL #####

# MCR v95 #####
echo "Install Matlab R2018b (v95) Runtime"
cd $HOME/Downloads

if [ ! -e 'MCR_R2018b_glnxa64_installer.zip' ]; then
  curl -O http://www.lin4neuro.net/lin4neuro/neuroimaging_software_packages/MCR_R2018b_glnxa64_installer.zip
fi

mkdir mcr
cd mcr
unzip ../MCR_R2018b_glnxa64_installer.zip

#Install to /usr/local/MATLAB/MCR/v95
sudo ./install -mode silent -agreeToLicense yes \
    -destinationFolder /usr/local/MATLAB/MCR/v95

# END OF MCR #####

source ~/.bashrc

# SPM12
Download SPM12 standalone
echo "Download SPM12 standalone"
cd $HOME/Downloads

if [ ! -e 'spm12_standalone.zip' ]; then
  curl -O http://www.lin4neuro.net/lin4neuro/neuroimaging_software_packages/spm12_standalone.zip
fi

cd /usr/local
sudo unzip ~/Downloads/spm12_standalone.zip
cd spm12_standalone
sudo chmod 755 run_spm12.sh spm12

#alias 
echo '' >> ~/.bash_aliases
echo '#SPM12 standalone' >> ~/.bash_aliases
echo "alias spm='/usr/local/spm12_standalone/run_spm12.sh /usr/local/MATLAB/MCR/v95'" >> ~/.bash_aliases

echo "Initialize SPM12 standalone"
sleep 5
sudo /usr/local/spm12_standalone/run_spm12.sh /usr/local/MATLAB/MCR/v95 ; \
sudo chown -R $(whoami):$(whoami) ~/.matlab



