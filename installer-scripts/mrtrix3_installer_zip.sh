#!/bin/bash
#Script to setup mrtrix3 for Ubuntu 20.04

# 26 May 2023 K. Nemoto

ver=focal

#Install prerequisite packages
echo "Begin installation of MRtrix3"
echo "Install prerequisite packages"
sudo apt-get install -y git g++ python-is-python3 libeigen3-dev zlib1g-dev libqt5opengl5-dev \
    libqt5svg5-dev libgl1-mesa-dev libfftw3-dev libtiff5-dev libpng-dev


cd $HOME/Downloads
if [ ! -e mrtrix3_${ver}.zip ]; then
  curl -O http://www.lin4neuro.net/lin4neuro/neuroimaging_software_packages/mrtrix3_${ver}.zip
fi

cd /usr/local
sudo unzip ~/Downloads/mrtrix3_${ver}.zip

#.bash_aliases
grep MRtrix3 ~/.bash_aliases > /dev/null
if [ $? -eq 1 ]; then
    echo '' >> ~/.bash_aliases
    echo '# MRtrix3' >> ~/.bash_aliases
    echo 'export PATH=$PATH:/usr/local/mrtrix3/bin' >> ~/.bash_aliases
fi

echo "Finished!"
sleep 5

exit

