#!/bin/bash

# Install 3D Slicer, ANTs, DSI Studio, FSL, MRtrix3, and SPM12

./3dslicer_installer.sh
./ants_installer.sh
./dsistudio_installer_focal.sh
./fsl_installer_archive.sh
./mrtrix3_installer_zip.sh
./spm12_installer_v99.sh

