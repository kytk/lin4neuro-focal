#!/bin/bash

# This is based on the link below from NVIDIA
# https://developer.nvidia.com/cuda-10.2-download-archive?target_os=Linux&target_arch=x86_64&target_distro=Ubuntu&target_version=1804&target_type=debnetwork

# Delete previous public key
sudo apt-key del 7fa2af80

cd $HOME/Downloads
if [ ! -e /etc/apt/preferences.d/cuda-repository-pin-600 ]; then
  wget https://developer.download.nvidia.com/compute/cuda/repos/ubuntu1804/x86_64/cuda-ubuntu1804.pin
  sudo mv cuda-ubuntu1804.pin /etc/apt/preferences.d/cuda-repository-pin-600
fi

if [ ! -e /etc/apt/sources.list.d/cuda-ubuntu1804-x86_64.list ]; then
  wget https://developer.download.nvidia.com/compute/cuda/repos/ubuntu1804/x86_64/cuda-keyring_1.0-1_all.deb
  sudo dpkg -i cuda-keyring_1.0-1_all.deb 
fi

sudo apt-get update
sudo apt-get -y install --no-install-recommends cuda-11-7 cuda-11-2
sudo apt-get -y install --no-install-recommends cuda-toolkit-10-2 

sudo update-alternatives --install /usr/local/cuda cuda /usr/local/cuda-10.2 102
sudo update-alternatives --install /usr/local/cuda cuda /usr/local/cuda-11.2 112
sudo update-alternatives --install /usr/local/cuda cuda /usr/local/cuda-11.7 117
sudo update-alternatives --auto cuda

sudo apt-mark hold cuda-11-7 cuda-toolkit-10-2 cuda-11-2

grep cuda-10.2  ~/.bash_aliases > /dev/null
if [ $? -eq 1 ]; then
    echo '' >> ~/.bash_aliases
    echo '# CUDA' >> ~/.bash_aliases
    echo 'export PATH=/usr/local/cuda/bin${PATH:+:${PATH}}' >> ~/.bash_aliases
    echo 'export LD_LIBRARY_PATH=/usr/local/cuda/lib64:/usr/local/cuda-10.2/lib64:/usr/local/cuda-11.2/lib64:/usr/local/cuda-11.7/lib64${LD_LIBRARY_PATH:+${LD_LIBRARY_PATH}}' >> ~/.bash_aliases
fi


