#!/bin/bash
# Script to install freesurfer
# This script downloads required files, install them, 
#  and configure that subject directory is under $HOME

# 27-Aug-2022 K. Nemoto

# Changelog
# 27-Aug-2022 use tar.gz instead of deb in order to allow co-existence of different version
# 21-Aug-2022 modify the script with the release of 7.3.2

# For debug
#set -x

## Variables #########################################################
ver=7.3.2
ubuntuver=20
tgz="freesurfer-linux-ubuntu${ubuntuver}_amd64-${ver}.tar.gz"
url="https://surfer.nmr.mgh.harvard.edu/pub/dist/freesurfer/${ver}/${tgz}"
curlcmd="curl -O -C -"
md5="de9e639976a480612cbba1cf5d33f2b0"
# md5 values can be obtained from the link below.
# https://surfer.nmr.mgh.harvard.edu/fswiki/rel7downloads
######################################################################


echo "Begin installation of FreeSurfer"
echo ""
echo "This script will download and install Freesurfer in Ubuntu"
echo "You need to prepare license.txt beforehand."
echo "license.txt should be placed in $HOME/Downloads"


while true; do

echo "Are you sure you want to begin the installation of FreeSurfer? (yes/no)"
read answer 
    case $answer in
        [Yy]*)
          echo "Begin installation."
	  break
          ;;
        [Nn]*)
          echo "Abort installation."
          exit 1
          ;;
        *)
          echo -e "Please type yes or no. \n"
          ;;
    esac
done


# Check if one wants to modify recon-all for VirtualBox environment
while true; do
echo "Do you want to modify recon-all for VirtualBox environment? (yes/no)"
read answer
    case $answer in
        [Yy]*)
          echo "modify recon-all later."
          reconallvb=1
          break
          ;;
        [Nn]*)
          echo "will not modify recon-all."
          break
          ;;
        *)
          echo -e "Please type yes or no. \n"
          ;;
    esac
done

echo "Check if you have license.txt in $HOME/Downloads"

if [ -e $HOME/Downloads/license.txt ]; then
    echo "license.txt exists. Continue installation."
else
    echo "You need to prepare license.txt"
    echo "Abort installation."
    echo "Please run the script after you get your license.txt" 
    echo "from the following window and put it in $HOME/Downloads"
    xdg-open "https://surfer.nmr.mgh.harvard.edu/registration.html" 
    exit 1
fi

cd $HOME/Downloads


# Download freesurfer
if [ ! -e $HOME/Downloads/${tgz} ]; then
	echo "Download Freesurfer to $HOME/Downloads"
	cd $HOME/Downloads
        eval $curlcmd $url
else
	echo "Freesurfer archive is found in $HOME/Downloads"
fi


# Check the archive
cd $HOME/Downloads
echo "Check if the downloaded archive is not corrupt."
echo "${md5} ${tgz}" > ${tgz}.md5
md5sum -c ${tgz}.md5
while [ "$?" -ne 0 ]; do
    echo "Filesize is not correct. Re-try downloading."
    sleep 5
    eval $curlcmd $url
    md5sum -c ${tgz}.md5
done

echo "Filesize is correct!"
rm ${tgz}.md5

# Install dependencies
echo "Install dependent packages"
sudo apt install -y language-pack-en binutils libx11-dev gettext x11-apps \
  perl make csh tcsh bash file bc gzip tar \
  xorg xorg-dev xserver-xorg-video-intel libncurses5 libbsd0 libc6 libc6 \
  libcom-err2 libcrypt1 libdrm2 libegl1 libexpat1 libffi7 libfontconfig1 \
  libfreetype6 libgcc-s1 libgl1 libglib2.0-0 libglu1-mesa libglvnd0 libglx0 \
  libgomp1 libgssapi-krb5-2 libice6 libjpeg62 libk5crypto3 libkeyutils1 \
  libkrb5-3 libkrb5support0 libpcre3 libpng16-16 libquadmath0 libsm6 \
  libstdc++6 libuuid1 libwayland-client0 libwayland-cursor0 libx11-6 \
  libx11-xcb1 libxau6 libxcb-icccm4 libxcb-image0 libxcb-keysyms1 \
  libxcb-randr0 libxcb-render-util0 libxcb-render0 libxcb-shape0 \
  libxcb-shm0 libxcb-sync1 libxcb-util1 libxcb-xfixes0 libxcb-xinerama0 \
  libxcb-xinput0 libxcb-xkb1 libxcb1 libxdmcp6 libxext6 libxft2 libxi6 \
  libxkbcommon-x11-0 libxkbcommon0 libxmu6 libxrender1 libxss1 libxt6 zlib1g  


# Install freesurfer
echo "Install freesurfer"
if [ ! -d /usr/local/freesurfer ]; then
  sudo mkdir /usr/local/freesurfer
fi

cd /usr/local/freesurfer/
sudo tar xvf $HOME/Downloads/${tgz}
sudo mv freesurfer $ver

#if [ -d "/usr/local/freesurfer/$ver" ]; then
#    sudo cp $HOME/Downloads/license.txt /usr/local/freesurfer/$ver
#else
#    echo "freesurfer is not installed correctly."
#    exit 1
#fi


# Prepare freesurfer directory in $HOME
echo "make freesurfer directory in $HOME"
cd $HOME

if [ ! -d $HOME/freesurfer/$ver/subjects ]; then
    mkdir -p $HOME/freesurfer/$ver/subjects
fi

cp -ar /usr/local/freesurfer/$ver/subjects $HOME/freesurfer/$ver/
cp $HOME/Downloads/license.txt $HOME/freesurfer/$ver/

# Append to .bash_aliases
if [ -f $HOME/.bash_aliases ]; then
  grep freesurfer/$ver $HOME/.bash_aliases > /dev/null
  if [ "$?" -eq 0 ]; then
    echo ".bash_aliases is already set."
  else
    echo >> $HOME/.bash_aliases
    echo "#FreeSurfer $ver" >> $HOME/.bash_aliases
    echo "export SUBJECTS_DIR=~/freesurfer/$ver/subjects" >> $HOME/.bash_aliases
    echo "export FREESURFER_HOME=/usr/local/freesurfer/$ver" >> $HOME/.bash_aliases
    echo "export FS_LICENSE=$HOME/freesurfer/$ver/license.txt" >> $HOME/.bash_aliases
    echo 'source $FREESURFER_HOME/SetUpFreeSurfer.sh' >> $HOME/.bash_aliases
  fi
fi


# Replace 'ln -s' and 'ln -sf' with 'cp' in recon-all, trac-preproc, gcaprepone, 
#  and make_average_{subject,surface,volume} for virtualbox environment
if [ "$reconallvb" == 1 ]; then
  sudo sed -i 's/ln -sf/cp/' /usr/local/freesurfer/$ver/bin/recon-all
  sudo sed -i 's/ln -s \$hemi/cp \$hemi/' /usr/local/freesurfer/$ver/bin/recon-all
  sudo sed -i 's/ln -s \$FREESURFER_HOME\/subjects\/fsaverage/cp -r \$FREESURFER_HOME\/subjects\/fsaverage \$SUBJECTS_DIR/' /usr/local/freesurfer/$ver/bin/recon-all
  sudo sed -i 's/ln -s \$FREESURFER_HOME\/subjects\/\${hemi}.EC_average/cp -r \$FREESURFER_HOME\/subjects\/\${hemi}.EC_average \$SUBJECTS_DIR/' /usr/local/freesurfer/$ver/bin/recon-all
#  sudo sed -i 's/ln -sfn/cp/' /usr/local/freesurfer/$ver/bin/trac-preproc
#  sudo sed -i 's/ln -sf/cp/' /usr/local/freesurfer/$ver/bin/trac-preproc
#  sudo sed -i 's/ln -s/cp/' /usr/local/freesurfer/$ver/bin/trac-preproc
  sudo sed -i 's/ln -s/cp/' /usr/local/freesurfer/$ver/bin/gcaprepone
  sudo sed -i 's/ln -s/cp/' /usr/local/freesurfer/$ver/bin/make_average_subject
  sudo sed -i 's/ln -s/cp/' /usr/local/freesurfer/$ver/bin/make_average_surface
  sudo sed -i 's/ln -s/cp/' /usr/local/freesurfer/$ver/bin/make_average_volume
fi

echo "Installation finished!"
echo "Now close this terminal, open another terminal, then run freeview."

exit

