#!/bin/bash

fslver=6.0.7.4
ubuntuver=focal

[[ -d $HOME/Downloads ]] || mkdir $HOME/Downloads

echo "Download FSL. It takes time, so please be patient."
cd $HOME/Downloads
if [ ! -e fsl-${fslver}-${ubuntuver}.tar.gz ]; then
  curl -O http://www.lin4neuro.net/lin4neuro/neuroimaging_software_packages/fsl-${fslver}-${ubuntuver}.tar.gz.md5
  curl -O -C - http://www.lin4neuro.net/lin4neuro/neuroimaging_software_packages/fsl-${fslver}-${ubuntuver}.tar.gz
fi

echo "Check the hashsum of fsl-${fslver}-${ubuntuver}.tar.gz"
    openssl md5 fsl-${fslver}-${ubuntuver}.tar.gz | cmp fsl-${fslver}-${ubuntuver}.tar.gz.md5 -
    while [ $? -ne 0 ]; do
      echo "Hashsum is different"
      echo "Retry download"
      rm fsl-${fslver}-${ubuntuver}.tar.gz.md5 fsl-${fslver}-${ubuntuver}.tar.gz
      curl -O http://www.lin4neuro.net/lin4neuro/neuroimaging_software_packages/fsl-${fslver}-${ubuntuver}.tar.gz.md5
      curl -O -C - http://www.lin4neuro.net/lin4neuro/neuroimaging_software_packages/fsl-${fslver}-${ubuntuver}.tar.gz
      openssl md5 fsl-${fslver}-${ubuntuver}.tar.gz | cmp fsl-${fslver}-${ubuntuver}.tar.gz.md5 -
    done


cd /usr/local
if [ -d /usr/local/fsl ]; then
  sudo mv fsl fsl_prev
fi

sudo tar xvzf ~/Downloads/fsl-${fslver}-${ubuntuver}.tar.gz

grep 'share/fsl/bin' ~/.profile > /dev/null
if [ $? -eq 1 ]; then

cat <<'EOF' >> ~/.profile

# FSL Setup
FSLDIR=/usr/local/fsl
PATH=${FSLDIR}/share/fsl/bin:${PATH}
export FSLDIR PATH
. ${FSLDIR}/etc/fslconf/fsl.sh
EOF

fi

echo "FSL was installed successfully. Please logout and login."

exit

