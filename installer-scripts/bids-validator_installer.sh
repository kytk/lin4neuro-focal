#!/bin/bash

# Install nvm
echo "Install nvm"
curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.39.2/install.sh | bash

# Install latest version of node
echo "Install node"
source ~/.bashrc
nvm install node

# Install bids-validator
echo "Install bids-validator"
npm install -g bids-validator

echo "Done"

