#!/bin/bash
# Installer script for AnyDesk

# 21 Aug 2022 K. Nemoto

wget -qO - https://keys.anydesk.com/repos/DEB-GPG-KEY | sudo apt-key add -

sudo sh -c 'echo "deb http://deb.anydesk.com/ all main" > /etc/apt/sources.list.d/anydesk-stable.list'

sudo apt-get update
sudo apt-get install -y anydesk
