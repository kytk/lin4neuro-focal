#!/bin/bash
#Install Matlab R2020b Runtime

zipfile=MATLAB_Runtime_R2020b_Update_8_glnxa64.zip

echo "Install Matlab R2020b (v99) Runtime"
cd $HOME/Downloads

if [ ! -e $zipfile ]; then
  curl -O http://www.lin4neuro.net/lin4neuro/neuroimaging_software_packages/${zipfile}
fi

mkdir mcr_v99
cd mcr_v99
unzip ../${zipfile}

#Install to /usr/local/MATLAB/MCR/v99
sudo ./install -mode silent -agreeToLicense yes \
    -destinationFolder /usr/local/MATLAB/MCR/v99

echo "Finished!"

exit

