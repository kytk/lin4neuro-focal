#!/bin/bash
# install script for ENIGMA Halfpipe on Ubuntu
# based on the instruction below
# https://github.com/HALFpipe/HALFpipe

set -x

cd $HOME/Downloads

if [[ ! -e halfpipe-halfpipe-latest.sif ]]; then
  curl -O https://download.fmri.science/singularity/halfpipe-halfpipe-latest.sif
fi

[[ -d /usr/local/halfpipe ]] || sudo mkdir /usr/local/halfpipe
sudo mv halfpipe-halfpipe-latest.sif /usr/local/halfpipe


exit

