#!/bin/bash
# Installer script for DSI studio

# Rename existing version
if [[ -d /usr/local/dsi-studio ]]; then
  sudo mv /usr/local/dsi-studio /usr/local/dsi-studio-prev
fi

archive=dsi_studio_ubuntu2004.zip

echo "Install necessary files"
sudo apt install -y git libboost-all-dev zlib1g zlib1g-dev

echo "Install Qt 5.12"
sudo apt install -y qt5-qmake qt5-default libqt5charts5-dev

echo "Install DSI Studio"
cd $HOME/Downloads

if [ ! -e $archive ]; then
  curl -O http://www.lin4neuro.net/lin4neuro/neuroimaging_software_packages/${archive}
fi

cd /usr/local
[[ -d dsi-studio ]] && sudo mv dsi-studio dsi-studio_prev
sudo unzip ~/Downloads/${archive}
sudo chmod 755 dsi-studio

#make icon show in the neuroimaging directory
sed -i 's/NoDisplay=true/NoDisplay=false/' ~/.local/share/applications/dsistudio.desktop

echo "Finished!"
sleep 5
exit

