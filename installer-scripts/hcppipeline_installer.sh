#!/bin/bash
# HCP Pipeline installer for Ubuntu 
# This script installs the following:
# - HCP Pipelines
# - Connectome Workbench
# - MSM
# - R packages for FIX
# - FIX
# - Gradunwarp
# FSL needs to be installed beforehand
# FreeSurfer 6.0 needs to be installede separately
#
# K. Nemoto 16 Oct 2022

set -x

# Log
log=$(date +%Y%m%d%H%M%S)-hcppipeline.log
exec &> >(tee -a "$log")

# Check if FSL is installed
if [ -z $FSLDIR ] ; then
  echo "please install FSL first."
  exit 1
fi


# HCP Pipeline
echo "Install HCP Pipeline scripts to $HOME/projects/Pipelines"
[ ! -d $HOME/git ] && mkdir $HOME/git
[ ! -d $HOME/projects ] && mkdir $HOME/projects
cd $HOME/git
git clone https://github.com/Washington-University/HCPpipelines.git
cp -r HCPpipelines $HOME/projects/Pipelines


# Connectome Workbench
## Check if neurodebian is setup
sudo apt-get update | tee apt.tmp
grep neurodebian apt.tmp > /dev/null
if [ $? -eq 1 ]; then
  echo "neurodebian repository is not set up yet, so set it up now."
  cd $HOME/Downloads
  # Neurodebian Public Key
  wget http://www.lin4neuro.net/lin4neuro/neuroimaging_software_packages/neurodebian.asc
  sudo cp neurodebian.asc /etc/apt/trusted.gpg.d/
  
  # apt
  # This sets neurodebian mirror server in Japan. 
  # Please change the address to your nearest server.
cat << EOS > neurodebian.sources.list
deb http://neuroimaging.sakura.ne.jp/neurodebian data main contrib non-free
deb http://neuroimaging.sakura.ne.jp/neurodebian $(lsb_release -cs) main contrib non-free
EOS
  sudo mv neurodebian.sources.list /etc/apt/sources.list.d/
  sudo apt-get update
fi

rm apt.tmp

echo "Install connectome workbench"
sudo apt-get install -y connectome-workbench


# MSM
echo "Install msm to /usr/local/fsl/bin"
cd $HOME/Downloads
wget https://github.com/ecr05/MSM_HOCR/releases/download/v3.0FSL/msm_ubuntu_v3
sudo cp msm_ubuntu_v3 /usr/local/fsl/bin
cd /usr/local/fsl/bin
sudo mv msm msm.orig
sudo mv msm_ubuntu_v3 msm
sudo chmod 755 msm


# R
echo "Install R 4.x and related packages needed for FIX"
# purge previous version
sudo apt-get purge -y r-base
sudo apt-get update
sudo apt-get install -y --no-install-recommends software-properties-common dirmngr
# Add public key
# To verify key, run gpg --show-keys /etc/apt/trusted.gpg.d/cran_ubuntu_key.asc 
# Fingerprint: E298A3A825C0D65DFD57CBB651716619E084DAB9
wget -qO- https://cloud.r-project.org/bin/linux/ubuntu/marutter_pubkey.asc | sudo tee -a /etc/apt/trusted.gpg.d/cran_ubuntu_key.asc
# Add R 4.0 repository
sudo add-apt-repository -y "deb https://cloud.r-project.org/bin/linux/ubuntu $(lsb_release -cs)-cran40/"
sudo apt-get update
# Install R 4.x
sudo apt-get install -y --no-install-recommends r-base

# Prepare $HOME/R for installation of packages
[ ! -d $HOME/R ] && mkdir $HOME/R
grep R_LIBS $HOME/.bash_aliases > /dev/null
if [ $? -eq 1 ]; then 
  echo '' >> $HOME/.bash_aliases
  echo '#R' >> $HOME/.bash_aliases
  echo 'export R_LIBS=$HOME/R' >> $HOME/.bash_aliases
  source $HOME/.bashrc
fi

# Install packages for devtools
sudo apt-get install -y libfreetype6-dev libfribidi-dev libharfbuzz-dev \
  git libxml2-dev make libfontconfig1-dev libicu-dev pandoc zlib1g-dev \
  libssl-dev libjpeg-dev libpng-dev libtiff-dev libgit2-dev \
  libcurl4-openssl-dev gfortran

# Install various R packages
cd $HOME/Downloads
cat << EOS > FIX_Rpackages.R
install.packages("devtools", lib="$HOME/R")
require(devtools)
install_version("kernlab", version="0.9-24", upgrade="never", lib="$HOME/R")
install_version("caTools", version="1.16", upgrade="never", lib="$HOME/R")
install_version("ROCR", version="1.0-7", upgrade="never", lib="$HOME/R")
install_version("class", version="7.3-14", upgrade="never", lib="$HOME/R")
install_version("mvtnorm", version="1.0-8", upgrade="never", lib="$HOME/R")
install_version("multcomp", version="1.4-8", upgrade="never", lib="$HOME/R")
install_version("e1071", version="1.6-7", upgrade="never", lib="$HOME/R")
install_version("randomForest", version="4.6-12", upgrade="never", lib="$HOME/R")
install_version("coin", version="1.2-2", upgrade="never", lib="$HOME/R")
install_version("party", version="1.0-25", upgrade="never", lib="$HOME/R")
EOS

Rscript FIX_Rpackages.R
rm FIX_Rpackages.R


# FIX
echo "Install FIX to /usr/local/fix"
cd $HOME/Downloads
if [ ! -e fix.tar.gz ]; then
  wget http://www.fmrib.ox.ac.uk/~steve/ftp/fix.tar.gz
fi
echo '417dd8f0e13a3a4c4a99852119a31330  fix.tar.gz' > fix.tar.gz.md5
md5sum -c fix.tar.gz.md5
while [ $? -ne 0 ]; do 
  echo "It seems the file is corrput."
  echo "Re-try downloading"
  mv fix.tar.gz fix.tar.gz.old
  wget http://www.fmrib.ox.ac.uk/~steve/ftp/fix.tar.gz
  md5sum -c fix.tar.gz.md5
done

[ -d fix ] mv fix fix.old
tar xvf fix.tar.gz
sudo mv fix /usr/local


# MCR v93
echo "Install MCR v93 for FIX"
cd $HOME/Downloads
curl -O http://www.lin4neuro.net/lin4neuro/neuroimaging_software_packages/MCR_R2017b_glnxa64_installer.zip
mkdir mcr_v93
mv MCR_R2017b_glnxa64_installer.zip mcr_v93
cd mcr_v93
unzip MCR_R2017b_glnxa64_installer.zip
sudo ./install -mode silent -agreeToLicense yes \
  -destinationFolder /usr/local/MATLAB/MCR/v93

# settings.sh for MCRv93
wget http://www.lin4neuro.net/lin4neuro/neuroimaging_software_packages/settings.sh.mcrv93
sudo mv settings.sh.mcrv93 /usr/local/fix
cd /usr/local/fix
sudo mv settings.sh settings.sh.orig
sudo mv settings.sh.mcrv93 settings.sh
sudo chmod 755 settings.sh

# Gradunwarp
echo "Install Gradunwarp to $HOME/.local/bin"
cd $HOME/git
git clone https://github.com/Washington-University/gradunwarp.git
cd gradunwarp
pip3 install numpy scipy pydicom nose sphinx nibabel
python3 setup.py install --prefix=$HOME/.local/bin

echo "Done"

exit

