#!/bin/bash

# Script to setup pyenv, which can switch python versions
# 08 Oct 2022 K. Nemoto

cwd=$PWD

echo "Check if Pyenv is installed already"
which pyenv > /dev/null
if [ $? -eq 0 ]; then
  echo "Pyenv is already installed."
  echo "Update pyenv instead"
  cd $(pyenv root)
  git pull
  echo "Updated"
  exit 0
fi
  
# Update APT
sudo apt-get update
  
# Install necessary packages for pyenv
sudo apt-get install -y \
  libffi-dev libssl-dev zlib1g-dev liblzma-dev tk-dev \
  libbz2-dev libreadline-dev libsqlite3-dev libopencv-dev

# Install suggested build environment
sudo apt-get install make build-essential libssl-dev zlib1g-dev \
  libbz2-dev libreadline-dev libsqlite3-dev wget curl llvm \
  libncursesw5-dev xz-utils tk-dev libxml2-dev libxmlsec1-dev \
  libffi-dev liblzma-dev

# Download pyenv
cd $HOME
which pyenv > /dev/null
if [ "$?" -ne 0 ]; then
  echo "Pyenv will be installed"
  git clone https://github.com/pyenv/pyenv.git ~/.pyenv
else
  echo "Pyenv is already installed."
fi

# Add PATH for pyenv to .bash_aliases
grep PYENV $HOME/.bash_aliases > /dev/null
if [ "$?" -eq 1 ]; then
cat << 'EOS' >> ~/.bash_aliases
   
#Pyenv
export PYENV_ROOT=$HOME/.pyenv
command -v pyenv >/dev/null || export PATH="$PYENV_ROOT/bin:$PATH"
eval "$(pyenv init -)"
EOS
  
fi

echo "Pyenv is successfully installed."

exit

