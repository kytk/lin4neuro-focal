#!/bin/bash
#CONN21a standalone installer

#Check MCR is installed
if [ ! -d /usr/local/MATLAB/MCR/v99 ]; then
  echo "Matlab Compiler Runtime needs to be installed first!"
  ~/git/lin4neuro-focal/installer-scripts/mcr_v99_installer.sh
fi

#Download CONN21a standalone
echo "Download CONN21a standalone"
cd $HOME/Downloads

if [ ! -e 'conn21a_standalone_ubuntu20_mcrv99.zip' ]; then
  curl -O http://www.lin4neuro.net/lin4neuro/neuroimaging_software_packages/conn21a_standalone_ubuntu20_mcrv99.zip
fi

cd /usr/local
sudo unzip ~/Downloads/conn21a_standalone_ubuntu20_mcrv99.zip

#Desktop entry
cat << EOS > ~/.local/share/applications/conn21a.desktop
[Desktop Entry]
Encoding=UTF-8
Name=CONN
Exec=bash -c '/usr/local/conn21a_standalone/run_conn.sh /usr/local/MATLAB/MCR/v99'
Icon=conn.png
Type=Application
Terminal=true
Categories=Neuroimaging;
NoDisplay=false
EOS

#alias
grep conn21a ~/.bash_aliases > /dev/null
if [ $? -eq 1 ]; then
    echo '' >> ~/.bash_aliases
    echo '#conn21a standalone' >> ~/.bash_aliases
    echo "alias conn='/usr/local/conn21a_standalone/run_conn.sh /usr/local/MATLAB/MCR/v99'" >> ~/.bash_aliases
fi

echo "Finished! Run CONN from menu -> Neuroimaging -> CONN"
sleep 5 
exit

