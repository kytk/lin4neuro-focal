#!/bin/bash

tgz='alizams-1.8.3_linux.tar.gz'

echo "Install Aliza MS"
cd $HOME/Downloads

if [ ! -e ${tgz} ]; then
  curl -O -C - http://www.lin4neuro.net/lin4neuro/neuroimaging_software_packages/${tgz}
fi

cd /usr/local

if [ -d alizams ]; then
  sudo rm -rf alizams
fi

sudo tar xvf $HOME/Downloads/${tgz}
sudo mv ${tgz%.tar.gz} alizams

if [ ! -e ~/.local/share/aaplications/alizams.desktop ]; then
  find $HOME -name 'alizams.desktop' 2>/dev/null -exec cp {} ~/.local/share/applications/ \;
fi

#make icon show in the neuroimaging directory
sed -i 's/NoDisplay=true/NoDisplay=false/' ~/.local/share/applications/alizams.desktop

#alias 
echo '' >> ~/.bash_aliases
echo '#Aliza MS' >> ~/.bash_aliases
echo "alias alizams='/usr/local/alizams/alizams.sh'" >> ~/.bash_aliases


echo "Finished!"
sleep 5
exit
