#!/bin/bash
#Script to setup mrtrix3 for Ubuntu 20.04

#Install prerequisite packages
echo "Begin installation of MRtrix3"
echo "Install prerequisite packages"
sudo apt-get install -y git g++ python-is-python3 libeigen3-dev zlib1g-dev libqt5opengl5-dev \
    libqt5svg5-dev libgl1-mesa-dev libfftw3-dev libtiff5-dev libpng-dev

#Download MRtrix3 source
echo "Download MRtrix3 source"
if [ ! -e $HOME/git ]; then
 mkdir $HOME/git
fi

cd $HOME/git
git clone https://github.com/MRtrix3/mrtrix3.git

#Configuration and build
echo "Configure and Build MRtrix3"
cd mrtrix3
./configure
./build
./package_mrtrix

sudo cp -ar _package/mrtrix3 /usr/local

#.bash_aliases
grep MRtrix3 ~/.bash_aliases > /dev/null
if [ $? -eq 1 ]; then
    echo '' >> ~/.bash_aliases
    echo '# MRtrix3' >> ~/.bash_aliases
    echo 'export PATH=$PATH:/usr/local/mrtrix3/bin' >> ~/.bash_aliases
fi

echo "Finished!"
sleep 5

exit

