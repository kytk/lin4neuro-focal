#!/bin/bash
#CONN20b standalone installer

#Check MCR is installed
if [ ! -d /usr/local/MATLAB/MCR/v95 ]; then
  echo "Matlab Compiler Runtime needs to be installed first!"
  ~/git/lin4neuro-focal/installer-scripts/mcr_v95_installer.sh
fi

#Download CONN20b standalone
echo "Download CONN20b standalone"
cd $HOME/Downloads

if [ ! -e 'conn20b_glnxa64.zip' ]; then
  curl -O http://www.lin4neuro.net/lin4neuro/neuroimaging_software_packages/conn20b_glnxa64.zip
fi

cd /usr/local
sudo mkdir conn20b_standalone
cd conn20b_standalone
sudo unzip ~/Downloads/conn20b_glnxa64.zip

#Desktop entry
cat << EOS > ~/.local/share/applications/conn20b.desktop
[Desktop Entry]
Encoding=UTF-8
Name=CONN
Exec=bash -c '/usr/local/conn20b_standalone/run_conn.sh /usr/local/MATLAB/MCR/v95'
Icon=conn.png
Type=Application
Terminal=false
Categories=Neuroimaging;
NoDisplay=false
EOS

#alias
echo '' >> ~/.bash_aliases
echo '#conn20b standalone' >> ~/.bash_aliases
echo "alias conn='/usr/local/conn20b_standalone/run_conn.sh /usr/local/MATLAB/MCR/v95'" >> ~/.bash_aliases


echo "Finished! Run CONN from menu -> Neuroimaging -> CONN"
sleep 5 
exit

