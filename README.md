# lin4neuro-focal

Building script of Lin4Neuro based on Ubuntu-mini 20.04 (focal)

This repository includes scripts to make Lin4Neuro from Ubuntu-mini.iso.
It also includes installer-scripts for several neuroimaging software.

# How to make Lin4Neuro with your own

## Install Ubuntu-mini.iso

You can get iso of ubuntu 20.04 from http://archive.ubuntu.com/ubuntu/dists/focal/main/installer-amd64/current/legacy-images/netboot/mini.iso

Install with this iso and select **no packages** during installation.

reboot the system and login.


## Install git and clone this repository

After installation, install git, make ~/git directory, and clone this repository under ~/git. (You can save this repository wherever you like.)

```
cd
LANG=C #if your LANG is other than English
sudo apt install git
mkdir git
cd git
git clone https://gitlab.com/kytk/lin4neuro-focal.git
```

## Install the Lin4Neuro-base

First, run **l4n-focal-1.sh**.

```
cd lin4neuro-focal
./l4n-focal-1.sh
```

The script will bring a prompt as following;

```
    Which language do you want to build? (English/Japanese)
    1) English
    2) Japanese
    3) quit
    #? 
```

Choose 1 or 2 depending on your language.

Then XFCE is installed. Upon installation, the system automatically reboots.

## Installation of Neuroimaging software packages

Then, run **l4n-focal-2.sh**.

```
cd ~/git/lin4neuro-focal
./l4n-focal-2.sh
```

This will install several neuroimaging software packages listed below.


## Check the software packages are installed correctly.

After installation of the software above, close and re-open the terminai.
Then, run **l4n-focal-check.sh**.

```
cd ~/git/lin4neuro-focal
./l4n-focal-check.sh
```

This script simply tries to run software listed above.
If it is not installed correctly, you will see error messages in the terminal. Check it and correct.


## Installer for other neuroimaging software packages

I also prepared the installer for popular software packages.

* AFNI
* ANTs
* CONN standalone
* FreeSurfer
* FSL
* MRtrix3
* SPM12 standalone 

these installer can be found in lin4neuro-focal/installer.


