#!/bin/bash

# Script to add /media/sf_share2

#fstab
echo '' | sudo tee -a /etc/fstab
echo '# Add another shared folder as /media/sf_share2' | sudo tee -a /etc/fstab
echo 'share2 /media/sf_share2 vboxsf _netdev,uid=1000,gid=1000 0 0' | sudo tee -a /etc/fstab

echo "Done. Please reboot the system to reflect the settings"

exit

