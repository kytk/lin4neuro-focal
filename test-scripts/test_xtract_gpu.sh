#!/bin/bash
# script to test extract
# Test scripts were provided by Dr. Tetsuo Koyama
# 26 Dec 2022 K. Nemoto

cd $HOME/Downloads

if [[ ! -e DTI_pipeline_L4N-GPU_20221226.tar.gz ]];then
  echo "Download a test archive"
  [[ -d DTI_pipeline_L4N-GPU_20221226 ]] && rm -rf DTI_pipeline_L4N-GPU_20221226
  curl -O http://www.lin4neuro.net/lin4neuro/neuroimaging_software_packages/DTI_pipeline_L4N-GPU_20221226.tar.gz
  tar xvzf DTI_pipeline_L4N-GPU_20221226.tar.gz
else
  echo "Found the test archive"
  [[ -d DTI_pipeline_L4N-GPU_20221226 ]] && rm -rf DTI_pipeline_L4N-GPU_20221226
  tar xvzf DTI_pipeline_L4N-GPU_20221226.tar.gz
fi

cd DTI_pipeline_L4N-GPU_20221226

# Run scripts
time ./automator.sh


