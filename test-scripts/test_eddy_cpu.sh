#!/bin/bash
# a script to test eddy_cuda
# 18 Dec 2022 K. Nemoto

cd $HOME/Downloads

if [ ! -e eddy_cpu_test.zip ];then
  echo "Download a test archive"
  [ -d eddy_cpu_test ] && rm -rf eddy_cpu_test
  curl -O http://www.lin4neuro.net/lin4neuro/neuroimaging_software_packages/eddy_cpu_test.zip
  unzip eddy_cpu_test.zip
else
  echo "Found the test archive"
  [ -d eddy_cpu_test ] && rm -rf eddy_cpu_test
  unzip eddy_cpu_test.zip
fi

cd eddy_cpu_test

# Run eddy_cpu
echo "Begin eddy_cpu"
time eddy_cpu --imain=dwidata \
     --mask=hifi_nodif_brain_mask \
     --index=index.txt \
     --acqp=acqparams.txt \
     --bvecs=bvecs \
     --bvals=bvals \
     --fwhm=0 \
     --topup=topup_AP_PA_b0 \
     --flm=quadratic \
     --out=eddy_unwarped_images \
     --data_is_shelled

# Check the output exists
if [ -e eddy_unwarped_images.nii.gz ]; then
  echo "eddy_cpu was done successfully."
  exit 0
else
  echo "ERROR: it seems eddy_cpu ended with some errors."
  exit 1
fi
