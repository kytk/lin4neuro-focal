#!/bin/bash

set -x

# prepare directory
cd ~/git
if [[ ! -d ANTS ]]; then
  mkdir -p ANTS/{build,install}
fi

if [[ -d ANTS/install ]]; then
  mv ANTS/install ANTS/install-prev
  mkdir ANTS/install
fi

# clone or update repository
cd ANTS
workingDir=${PWD}
if [[ ! -d ANTs ]]; then
  git clone https://github.com/ANTsX/ANTs.git
else
  cd ANTs
  git pull
fi

# build
cd ${workingDir}/build
cmake \
    -DCMAKE_INSTALL_PREFIX=${workingDir}/install \
    ../ANTs 2>&1 | tee cmake.log
make -j 4 2>&1 | tee build.log

# install
cd ANTS-build
make install 2>&1 | tee install.log

# publicize
cd ${workingDir}/install
mkdir ANTs
mv bin ANTs
cp ${workingDir}/ANTs/Scripts/* ${workingDir}/install/ANTs/bin/
cd ANTs
timestamp=$(date +%F)
echo "Compiled on $timestamp by K.Nemoto with the circumstance below" >> README.txt
echo '' >> README.txt
echo '$ uname -a' >> README.txt
echo '' >> README.txt
uname -a >> README.txt
echo '' >> README.txt
echo '$ cat /etc/lsb-release' >> README.txt
cat /etc/lsb-release >> README.txt

cd ..
zip -r ANTs.zip ANTs/

echo 'done'

exit

